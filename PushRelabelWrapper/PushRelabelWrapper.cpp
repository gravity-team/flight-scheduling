﻿// This is the main DLL file.

#include "stdafx.h"
#include "PushRelabelWrapper.h"

PushRelabelWrapper::CPushRelabelWrapper::CPushRelabelWrapper(int numberOfVertices) {
	pushRelabel = new PushRelabel(numberOfVertices);
}
/// <summary> Add edge to graph
/// <param name="vertexFrom> đỉnh bắt đầu của cạnh </param>
/// <param name="vertexTo> đỉnh kết thúc của cạnh </param>
/// <param name="capacity> sức chứa (khả năng thông qua) của cạnh </param>
/// </summary>
// add edge
void PushRelabelWrapper::CPushRelabelWrapper::AddEdge(int vertexFrom, int vertexTo, int capacity) {
	pushRelabel->AddEdge(vertexFrom, vertexTo, capacity);
}

int PushRelabelWrapper::CPushRelabelWrapper::GetMaxFlow(int source, int sink) {
	return pushRelabel->GetMaxFlow(source, sink);
}

int* PushRelabelWrapper::CPushRelabelWrapper::GetPlan() {
	return pushRelabel->GetPlan();
}


void PushRelabelWrapper::CPushRelabelWrapper::FreeMemory(int* pointer) {
	delete[] pointer;
}