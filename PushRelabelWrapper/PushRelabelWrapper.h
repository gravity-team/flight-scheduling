﻿// PushRelabelWrapper.h

#pragma once

//#ifndef _PUSH_RELABEL_WRAPPER_H_
//#define _PUSH_RELABEL_WRAPPER_H_

//#include "C:\Users\Admin\Desktop\MyLearning\HCĐH\HK1\Graph Theory\Project\CPushRelabel\CPushRelabel.h"
#include "CPushRelabel.h"

using namespace System;

namespace PushRelabelWrapper {

	public ref class CPushRelabelWrapper
	{
		// TODO: Add your methods for this class here.
	private:
		PushRelabel* pushRelabel;
	public:
		CPushRelabelWrapper(int numbernumberOfVertices);

		/// <summary> Add edge to graph
		/// <param name="vertexFrom> đỉnh bắt đầu của cạnh </param>
		/// <param name="vertexTo> đỉnh kết thúc của cạnh </param>
		/// <param name="capacity> sức chứa (khả năng thông qua) của cạnh </param>
		/// </summary>
		// add edge
		void AddEdge(int vertexFrom, int vertexTo, int capacity);

		int GetMaxFlow(int source, int sink);
		
		int* GetPlan();
		
		void FreeMemory(int* pointer);
	};
}

//#endif