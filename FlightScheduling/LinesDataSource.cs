﻿using FlightScheduling.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace FlightScheduling
{
    /// <summary>
    /// This acts as a DataSource for the example. As you can see, its returning instances of the
    /// LineViewModel class, which actually contains regular int and double properties.
    /// Therefore, it could be perfectly replaced by a DB data source, 
    /// with no need to modify any part of the XAML or the rest of the applicaton.
    /// </summary>
    public class LinesDataSource
    {
        public static Random rnd = new Random();

        public static List<LineViewModel> GetRandomLines()
        {
            return Enumerable.Range(0, rnd.Next(10, 20)).Select(GetRandomLineDemo).ToList();
        }

        public static LineViewModel GetRandomLineDemo(int index)
        {
            var colors = typeof(Colors).GetProperties().Select(x => (Color)x.GetValue(null, null)).ToList();

            return new LineViewModel()
                {
                    Name = "Line" + index.ToString(),
                    X1 = rnd.Next(0, 500),
                    X2 = rnd.Next(0, 500),
                    Y1 = rnd.Next(0, 500),
                    Y2 = rnd.Next(0, 500),
                    Opacity = .7,
                    Color1 = colors[rnd.Next(0, colors.Count - 1)],
                    Color2 = colors[rnd.Next(0, colors.Count - 1)],
                    Thickness = rnd.Next(5, 10),
                    AnimationSpeed = 5,
                    Animate = false
                };
        }

        public static FlyLineViewModel GetRandomLine(int index)
        {
            var colors = typeof(Colors).GetProperties().Select(x => (Color)x.GetValue(null, null)).ToList();

            return new FlyLineViewModel()
            {
                Name = "Line" + index.ToString(),
                X1 = rnd.Next(0, 500),
                X2 = rnd.Next(0, 500),
                Y1 = rnd.Next(0, 500),
                Y2 = rnd.Next(0, 500),
                Opacity = .7,
                Color1 = colors[rnd.Next(0, colors.Count - 1)],
                Color2 = colors[rnd.Next(0, colors.Count - 1)],
                Thickness = rnd.Next(5, 10),
                AnimationSpeed = 5,
                Animate = false
            };
        }

        public static FlyLineViewModel GetFlyLine(int index,double x1 = 0,
            double y1 = 0, double x2 = 0, double y2 = 0, double x2Des = 0, double y2Des = 0)
        {
            var colors = typeof(Colors).GetProperties().Select(x => (Color)x.GetValue(null, null)).ToList();

            return new FlyLineViewModel()
            {
                Name = "Line" + index.ToString(),
                X1 = x1,
                X2 = x2,
                Y1 = y1,
                Y2 = y2,
                X2Des = x2Des,
                Y2Des = y2Des,
                Opacity = .9,
                //Color1 = colors[rnd.Next(0, colors.Count - 1)],
                //Color2 = colors[rnd.Next(0, colors.Count - 1)],
                Color1 = Colors.Black,
                Color2 = Colors.Black,
                //Thickness = rnd.Next(5, 10),
                Thickness = 2,
                AnimationSpeed = 10,
                Animate = false
            };
        }
    }
}
