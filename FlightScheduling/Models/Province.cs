﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.Models
{
    public class Province : Model
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float X { get; set; }
        public float Y { get; set; }

        public IList<FlightsProvincesModel> FlightsProvincesModels { get; set; }
    }
}
