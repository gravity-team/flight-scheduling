﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.Models
{
    public class Model : INotifyPropertyChanged
    {
        #region Fields
        //private int _ProjectId;
        #endregion

        #region Properties
        //public int ProjectId
        //{
        //    get { return _ProjectId; }

        //    #region set
        //    set
        //    {
        //        if (_ProjectId != value)
        //        {
        //            _ProjectId = value;

        //            RaisePropertyChanged("ProjectId");
        //        }
        //    }
        //    #endregion
        //}
        #endregion

        #region Constructors
        //public Project() : base() { }

        //public Project(int _ProjectId, string _ProjectName, DateTime _Deadline)
        //{
        //    this._ProjectId = _ProjectId;
        //    this._ProjectName = _ProjectName;
        //    this._Deadline = _Deadline;
        //    this._Status = false;
        //}
        #endregion

        #region Navigation
        //// Collection navigation fields
        //private IList<Task> _Tasks;

        //// Collection navigation properties
        //public IList<Task> Tasks
        //{
        //    get { return _Tasks; }

        //    set
        //    {
        //        _Tasks = value;

        //        RaisePropertyChanged("Tasks");
        //    }
        //}
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
