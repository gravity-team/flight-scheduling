﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.Models
{
    public class Flight : Model
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int? PlaneId { get; set; }
        public int Cost { get; set; }

        public IList<FlightsProvincesModel> FlightsProvincesModels { get; set; }
        public Plane Plane { get; set; }
    }
}
