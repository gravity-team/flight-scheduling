﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightScheduling.Models
{
    public class FlightsProvincesModel : Model
    {
        public int Id { get; set; }
        public int ProvinceId { get; set; }
        public int FlightId { get; set; }
        public bool IsSource { get; set; }

        public Flight Flight { get; set; }
        public Province Province { get; set; }
    }
}
