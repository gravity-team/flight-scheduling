﻿using FlightScheduling.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FlightScheduling.Services
{
    public class GraphService
    {
        public static List<List<int>> RunAlgorithm()
        {
            List<Flight> flights;
            List<FlightsProvincesModel> flightProvinceModels;
            Dictionary<int, int> hashes = new Dictionary<int, int>();
            using (var db = new Context())
            {
                flights = db.Flights.ToList();
                flightProvinceModels = db.FlightsProvincesModels.ToList();
            }
            int N = flights.Count;
            PushRelabelWrapper.CPushRelabelWrapper graph = new PushRelabelWrapper.CPushRelabelWrapper(2 * N + 2);

            for (int i = 0; i < N; i++)
            {
                hashes.Add(flights[i].Id, i + 1);
            }

            foreach (var f in flights)
            {
                /* Đồ thị có 4 lớp
                 *                 /- Đỉnh 1            Đỉnh 6 (ảo của 1)  -\
                 *                /-- Đỉnh 2            Đỉnh 7 (ảo của 2)  --\
                 *      Đỉnh 0 --<--- Đỉnh 3            Đỉnh 8 (ảo của 3)  --->----- Đỉnh 11 (đích ảo)
                 *                \-- Đỉnh 4            Đỉnh 9 (ảo của 4)  --/
                 *                 \- Đỉnh 5            Đỉnh 10 (ảo của 5) -/
                 * 
                 * Nếu flight i bay đến flight j được, thì thêm cạnh từ i đến (j+N) tức là đỉnh ảo của j ở lớp 3
                 */
                graph.AddEdge(0, hashes[f.Id], 1);
                graph.AddEdge(hashes[f.Id] + N, 2 * N + 1, 1); // thêm cạnh dẫn từ ảo đến đích)
                var currentDestination = flightProvinceModels.Where(fpm => fpm.FlightId == f.Id && !fpm.IsSource).FirstOrDefault();
                var listRelatedDestination = flightProvinceModels.Where(fpm => fpm.ProvinceId == currentDestination.ProvinceId && fpm.IsSource).ToList();
                foreach (var rd in listRelatedDestination)
                {
                    if (f.EndTime <= rd.Flight.StartTime)
                    {
                        graph.AddEdge(hashes[f.Id], hashes[rd.Flight.Id] + N, 1); // nối với đỉnh ảo của rd.
                    }
                }
            }

            int maxFlow = graph.GetMaxFlow(0, 2 * N + 1);

            // cách lấy ra từng chuyến bay
            List<List<int>> plan = new List<List<int>>();
            unsafe
            {
                int* planPtr = graph.GetPlan();
                int numOfPlans = planPtr[0];
                int id = 1;
                for (int i = 0; i < numOfPlans; i++)
                {
                    int num = planPtr[id++];
                    List<int> curPlan = new List<int>();
                    for (int j = 0; j < num; j++)
                        curPlan.Add(planPtr[id++]);
                    plan.Add(curPlan);
                }

                graph.FreeMemory(planPtr);
            }

            return plan;
        }

        public static List<Flight> SetPlaneForFlights(List<List<int>> plans)
        {
            List<Flight> result = new List<Flight>();
            List<Plane> planes = new List<Plane>();

            using (var db = new Context())
            {
                result = db.Flights.Include(fl => fl.FlightsProvincesModels.Select(fpm => fpm.Province))
                                   .ToList();
                planes = db.Planes.ToList();
            }

            int plane = 0;

            foreach (var plan in plans)
            {
                if (planes[plane].Quantity > 0)
                {
                    planes[plane].Quantity--;
                }
                else
                {
                    planes[++plane].Quantity--;
                }

                foreach (var flight in plan)
                {
                    var fl = result.Single(f => f.Id == flight);

                    fl.Plane = new Plane
                    {
                        Id = planes[plane].Id,
                        Name = planes[plane].Name + " " + (plane + 1).ToString(),
                    };

                    fl.PlaneId = plane; // dùng đở làm số thứ tự
                }
            }

            return result;
        }
    }
}
