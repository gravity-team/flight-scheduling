﻿using FlightScheduling.Models;
using FlightScheduling.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FlightScheduling.Views
{
    class ComboboxItem
    {
        public string Text { get; set; }
        public int Value { get; set; }
        public ComboboxItem(string text, int value)
        {
            Text = text;
            Value = value;
        }
    }

    class FlightPlans
    {
        public int ChuyenBay { get; set; }
        public string DiemDi { get; set; }
        public string DiemDen { get; set; }
        public FlightPlans(int stt, string diemDi, string diemDen)
        {
            ChuyenBay = stt;
            DiemDi = diemDi;
            DiemDen = diemDen;
        }
    }
    /// <summary>
    /// Interaction logic for SCR002_AddGoogleMap.xaml
    /// </summary>
    public partial class SCR002_AddGoogleMap : Window
    {
        String sURL = AppDomain.CurrentDomain.BaseDirectory;
        String sUriTaget = "";

        public SCR002_AddGoogleMap()
        {
            InitializeComponent();
            //webBrowser.Navigate("http://www.maps.google.com");
            sURL = sURL.Substring(0, sURL.Length - 10);

        }

        private void setupObjectForScripting(object sender, RoutedEventArgs e)
        {
            ((WebBrowser)sender).ObjectForScripting = new HtmlInteropInternalTestClass();
        }

        List<Tuple<string, float, float, bool>> listBtnpoint = new List<Tuple<string, float, float, bool>>();
        List<List<int>> plans = new List<List<int>>();
        List<Flight> flights = new List<Flight>();
        private void btnShowWay_Click(object sender, RoutedEventArgs e)
        {
            plans = GraphService.RunAlgorithm();
            flights = GraphService.SetPlaneForFlights(plans); // set máy bay cho chuyến bay

            for (int i = 0; i < plans.Count; i++)
            {
                cboLichTrinhBay.Items.Add(new ComboboxItem("Flight plan " + (i + 1), i));

            }
            cboLichTrinhBay.DisplayMemberPath = "Text";
            cboLichTrinhBay.SelectedValuePath = "Value";
            string line = "";

            #region line string
            //Init
            line =
@"<!-- saved from url=(0014)about:internet -->
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"" />
    <meta name=""viewFlight"" content=""initial-scale=1.0, user-scalable=no"">
    <meta charset=""UTF-8"">
    <title>Google Maps JavaScript API v3 Example: Directions Complex</title>";
            #region CSS
            line = string.Concat(line, @"  
        <style type=""text/css"">
            .buttonCustom {
	            -moz-box-shadow:inset 0px 0px 0px 2px #fce2c1;
	            -webkit-box-shadow:inset 0px 0px 0px 2px #fce2c1;
	            box-shadow:inset 0px 0px 0px 2px #fce2c1;
	            background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ffc477), color-stop(1, #bd8744) );
	            background:-moz-linear-gradient( center top, #ffc477 5%, #bd8744 100% );
	            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#bd8744');
	            background-color:#ffc477;
	            -webkit-border-top-left-radius:8px;
	            -moz-border-radius-topleft:8px;
	            border-top-left-radius:8px;
	            -webkit-border-top-right-radius:8px;
	            -moz-border-radius-topright:8px;
	            border-top-right-radius:8px;
	            -webkit-border-bottom-right-radius:8px;
	            -moz-border-radius-bottomright:8px;
	            border-bottom-right-radius:8px;
	            -webkit-border-bottom-left-radius:8px;
	            -moz-border-radius-bottomleft:8px;
	            border-bottom-left-radius:8px;
	            text-indent:0px;
	            border:2px solid #eeb44f;
	            display:inline-block;
	            color:#ffffff;
	            font-family:Arial;
	            font-size:13px;
	            font-weight:bold;
	            font-style:normal;
	            height:40px;
	            line-height:40px;
	            width:200px;
                max-width: 100%;
	            text-decoration:none;
	            text-align:center;
	            text-shadow:1px 1px 0px #cc9f52;
            }
            .buttonCustom:hover {
	            background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #bd8744), color-stop(1, #ffc477) );
	            background:-moz-linear-gradient( center top, #bd8744 5%, #ffc477 100% );
	            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#bd8744', endColorstr='#ffc477');
	            background-color:#bd8744;
            }.buttonCustom:active {
	            position:relative;
	            top:1px;
            }
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #map_canvas {
            height: 100%;
            width: 100%;
            float: right;
        }

        #map_showOption {
            height: 30%;
            width: 15%;
            float: right;
        }

        #directionsPanel {
            height: 70%;
            overflow: auto;
            background: #0F4D87;
            color: white;
            width: 20%;
        }

        #control {
            background: #fff;
            padding: 5px;
            font-size: 14px;
            font-family: Arial;
            border: 1px solid #ccc;
            box-shadow: 0 2px 2px rgba(33, 33, 33, 0.4);
            display: none;
        }

        @media print {
            #map-canvas {
                width: 70%;
                margin: 0;
            }

            #directionsPanel {
                width: 20%;
            }
        }

        .adp-placemark {
            background-color: #1C7DD7;
        }

        .adp-legal {
            color: black;
        }

        .warnbox-content {
            background: white;
            color: black;
        }
    </style>");
            #endregion

            line = string.Concat(line, @"
    <script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?language=us'></script>
    <script type=""text/javascript"">
        var directionsDisplay;
        var directionsService = new google.maps.DirectionsService();
        var markersArray = [];
        var lineArr = [];
        var map;");
            line = string.Concat(line, @"       
        function initialize() {
            directionsDisplay = new google.maps.DirectionsRenderer();
            var myOptions = {
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: new google.maps.LatLng(16, 106),
            }
            map = new google.maps.Map(document.getElementById(""map_canvas""), myOptions);
            directionsDisplay.setMap(map);
            var trafficLayer = new google.maps.TrafficLayer();
            trafficLayer.setMap(map);

            //calcRoute();
            directionsDisplay.setPanel(document.getElementById(""directionsPanel""));
        }
        function clearMarkers() {
            for (var i = 0; i < markersArray.length; i++ ) 
            {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
            markersArray = [];
        }
        function clearLines() {
            for (var i = 0; i < lineArr.length; i++ ) 
            {
                lineArr[i].setMap(null);
            }
            lineArr.length = 0;
            lineArr = [];
        }
        function calcRoute() {
            clearMarkers();
            //Tao các điểm trên bản đồ (marker)
            // {Show MarkerBegin}// {Show MarkerEnd}" + Environment.NewLine);

            line = string.Concat(line, @"}
        function ShowLineWay() {
                    clearLines();
//                    var lineSymbol = {
//                      path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
//                    };
                    //Tạo line trên bản đồ
                    // {Show LinesBegin}// {Show LinesEnd}");

            line = string.Concat(line, @"
                    lineAnimate(lineArr,0);
                }
        function lineAnimate(lineArr,indexLine){
                var line  = lineArr[indexLine];        
                var X1 = line.getPath().b[0].lat();
                var Y1 = line.getPath().b[0].lng();
                var X2 = line.getPath().b[1].lat();
                var Y2 = line.getPath().b[1].lng();
                var step = 0;
                var numSteps = 250; //Change this to set animation resolution
                var timePerStep = 5; //Change this to alter animation speed
                var interval = setInterval(function() {
                    step += 1;
                    if (step > numSteps) {
                        clearInterval(interval);
                        if(lineArr.length <= indexLine + 1)
                            return;
                        lineAnimate(lineArr,indexLine + 1);
                    } else {
                        var departure =  new google.maps.LatLng(X1, Y1); //Set to whatever lat/lng you need for your departure location
                        var arrival = new google.maps.LatLng(X2, Y2); //Set to whatever lat/lng you need for your arrival location
                        var are_we_there_yet = google.maps.geometry.spherical.interpolate(departure,arrival,step/numSteps);
                        line.setPath([departure, are_we_there_yet]);
                        line.setMap(map);
                    }
                }, timePerStep);}
        function resetMap() {
            clearMarkers();
            clearLines();
        }
    </script>
</head>");
            line = string.Concat(line, @"
<body onload=""initialize()"">
    <div>
        <input type=""button"" class=""buttonCustom"" value=""Show Destination"" onclick=""calcRoute()"" />
        <input type=""button"" class=""buttonCustom"" value=""Show Flights Way"" onclick=""ShowLineWay()"" />
        <input type=""button"" class=""buttonCustom"" value=""Reset Map"" onclick=""resetMap()"" />
    </div>
    <div id=""map_canvas""></div>");

            line = string.Concat(line, @"
    <!--<div id=""directionsPanel"">
        <strong>Mode of Travel: </strong>
        <select id=""mode"" onchange=""calcRoute();"">
            <option value=""DRIVING"">Driving</option>
            <option value=""WALKING"">Walking</option>
            <option value=""BICYCLING"">Bicycling</option>
        </select>
    </div>-->
</body>
</html>");
            #endregion

            StreamWriter page = File.CreateText(sURL + "html\\map1.html");
            page.Write(line);
            page.Close();
            Uri uri = new Uri(sURL + "html\\map1.html");
            webBrowser1.Navigate(uri);
        }
        private void cboLichTrinhBay_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

            if (cboLichTrinhBay.SelectedIndex < 0)
                return;
            int numberDes = 1;
            string showMarkerString = string.Empty;
            string showLinesString = string.Empty;
            listBtnpoint = new List<Tuple<string, float, float, bool>>();

            int plIndex = int.Parse(cboLichTrinhBay.SelectedValue.ToString());
            dtg_Plans.Items.Clear();
            string _diemDi = string.Empty;
            string _diemDen = string.Empty;
            for (int i = 0; i < plans[plIndex].Count; i++)
            {
                var _provinces = flights[plans[plIndex][i]].FlightsProvincesModels;
                foreach (var item in _provinces)
                {
                    if (item.IsSource)
                        _diemDi = item.Province.Name;
                    else
                        _diemDen = item.Province.Name;
                }
                dtg_Plans.Items.Add(new FlightPlans(i + 1, _diemDi, _diemDen));
            }
            //lay toa do cac diem de phuc vu danh dau marker tren ban do
            for (int i = 0; i < plans[plIndex].Count; i++)
            {
                var prCurSource = flights[plans[plIndex][i]].FlightsProvincesModels
                                                    .Where(fpm => fpm.IsSource).FirstOrDefault().Province;
                var prCurGoal = flights[plans[plIndex][i]].FlightsProvincesModels
                                                .Where(fpm => !fpm.IsSource).FirstOrDefault().Province;
                if (i > 0)
                {
                    var prPrevGoal = flights[plans[plIndex][i - 1]].FlightsProvincesModels
                                                        .Where(fpm => !fpm.IsSource).FirstOrDefault().Province;

                    //Neu nhu diem khoi dau chuyen bay hien tai khac diem den chuyen bay truoc do
                    if (prPrevGoal.Id != prCurSource.Id)
                    {
                        listBtnpoint.Add(new Tuple<string, float, float, bool>(prCurSource.Name, prCurSource.X, prCurSource.Y, false));
                        listBtnpoint.Add(new Tuple<string, float, float, bool>(prCurGoal.Name, prCurGoal.X, prCurGoal.Y, true));

                    }
                    else
                    {
                        listBtnpoint.Add(new Tuple<string, float, float, bool>(prCurGoal.Name, prCurGoal.X, prCurGoal.Y, true));
                    }
                }
                else
                {
                    listBtnpoint.Add(new Tuple<string, float, float, bool>(prCurSource.Name, prCurSource.X, prCurSource.Y, true));
                    listBtnpoint.Add(new Tuple<string, float, float, bool>(prCurGoal.Name, prCurGoal.X, prCurGoal.Y, true));
                }
            }

            #region add marker and line to map
            foreach (var item in listBtnpoint)
            {
                showMarkerString += @"
                    var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                    var markerP" + numberDes + @" = new google.maps.Marker({
                        label :'" + numberDes + @"',
                        animation: google.maps.Animation.DROP,
                        position: new google.maps.LatLng(" + item.Item2 + "," + item.Item3 + @"),
                        //icon:'http://labs.google.com/ridefinder/images/mm_20_green.png',
                        map: map
                    });
                    var contentString" + numberDes + @" = '<div id=""content" + numberDes + @""">'+
                                        '<div id=""siteNotice" + numberDes + @""">'+
                                        '</div>'+
                                        '<h3 id=""country" + numberDes + @""" class=""firstHeading"">" + item.Item1 + @"</h3>'+
                                        '<div id=""bodyContent" + numberDes + @""">'+
                                        '<p><b>" + item.Item1 + @", Việt Nam</b></p>'+
                                        '</div>'+
                                        '</div>';
                    var infowindow" + numberDes + @" = new google.maps.InfoWindow({
                      content: contentString" + numberDes + @"
                    });
                    markerP" + numberDes + @".addListener('click', function() {
                      infowindow" + numberDes + @".open(map, markerP" + numberDes + @");
                    });
                    markersArray.push(markerP" + numberDes + @");";
                numberDes++;
            }
            for (int i = 1; i <= numberDes - 2; i++)
            {
                showLinesString = string.Concat(showLinesString, @"
                    var line" + i + @" = new google.maps.Polyline({
                        path: [
                            new google.maps.LatLng(" + listBtnpoint[i - 1].Item2 + ", " + listBtnpoint[i - 1].Item3 + @"),
                            new google.maps.LatLng(" + listBtnpoint[i].Item2 + ", " + listBtnpoint[i].Item3 + @")
                        ],");
                if (listBtnpoint[i - 1].Item4 == true && listBtnpoint[i].Item4 == false)
                {
                    showLinesString = string.Concat(showLinesString, @"strokeColor: ""#00b54b"",
                                                    strokeOpacity: 1.0,
                                                    strokeWeight: 2,");
                }
                else
                {
                    showLinesString = string.Concat(showLinesString, @"strokeColor: ""#FF0000"",
                                                    strokeOpacity: 1.0,
                                                    strokeWeight: 2,");
                }

                showLinesString = string.Concat(showLinesString, @"
                        geodesic: true, 
                        //map: map //Add vao map
                    });
                    lineArr.push(line" + i + @");");
            }
            #endregion

            if (File.Exists(sURL + "html\\map1.html"))
            {
                try
                {
                    //Thay thế chuỗi được đánh dấu
                    //Add lại list địa điểm khi lịch trình bay thay đổi
                    StreamReader objReader = new StreamReader(sURL + "html\\map1.html");
                    string line = "";
                    line = objReader.ReadToEnd();
                    objReader.Close();
                    int markerBegin = line.LastIndexOf("// {Show MarkerBegin}") + ("// {Show MarkerBegin}").Length;
                    int markerEnd = line.IndexOf("// {Show MarkerEnd}");
                    line = line.Remove(markerBegin, markerEnd - markerBegin);
                    line = line.Insert(markerBegin, showMarkerString);
                    int lineBegin = line.IndexOf("// {Show LinesBegin}") + ("// {Show LinesBegin}").Length;
                    int lineEnd = line.IndexOf("// {Show LinesEnd}");
                    line = line.Remove(lineBegin, lineEnd - lineBegin);
                    line = line.Insert(lineBegin, showLinesString);

                    StreamWriter page = File.CreateText(sURL + "html\\map1.html");
                    page.Write(line);
                    page.Close();
                    Uri uri = new Uri(sURL + "html\\map1.html");
                    webBrowser1.Navigate(uri);
                }
                catch (Exception)
                {

                    return;
                }

            }
        }
    }
    // Object used for communication from JS -> WPF
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class HtmlInteropInternalTestClass
    {
    }
}
