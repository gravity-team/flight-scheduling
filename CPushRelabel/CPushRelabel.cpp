﻿#include "CPushRelabel.h"

Edge::Edge(int vertexFrom, int vertexTo, int capacity, int flowValue, int edgeIndex) :
	VertexFrom(vertexFrom), VertexTo(vertexTo), Capacity(capacity), FlowValue(flowValue), EdgeIndex(edgeIndex) {}

PushRelabel::PushRelabel(int numberOfVertices) : NumberOfVertices(numberOfVertices) {
	MaxFlowResult = -1;
	if (numberOfVertices) {
		Graph.resize(NumberOfVertices);
		Excess.resize(NumberOfVertices);
		Distance.resize(NumberOfVertices);
		Active.resize(2 * NumberOfVertices);
		Count.resize(2 * NumberOfVertices);
	}
}

void PushRelabel::AddEdge(int vertexFrom, int vertexTo, int capacity) {
	Graph[vertexFrom].push_back(Edge(vertexFrom, vertexTo, capacity, 0, Graph[vertexTo].size()));
	if (vertexFrom == vertexTo) Graph[vertexFrom].back().EdgeIndex++;
	Graph[vertexTo].push_back(Edge(vertexTo, vertexFrom, 0, 0, Graph[vertexFrom].size() - 1));
}

void PushRelabel::Enqueue(int vertex) {
	if (!Active[vertex] && Excess[vertex] > 0) {
		Active[vertex] = true;
		Queue.push(vertex);
	}
}

void PushRelabel::Push(Edge &edge) {
	int flowValue = std::min(Excess[edge.VertexFrom], edge.Capacity - edge.FlowValue);
	if (Distance[edge.VertexFrom] <= Distance[edge.VertexTo] || flowValue == 0)
		return;
	edge.FlowValue += flowValue;
	Graph[edge.VertexTo][edge.EdgeIndex].FlowValue -= flowValue;
	Excess[edge.VertexFrom] -= flowValue;
	Excess[edge.VertexTo] += flowValue;
	Enqueue(edge.VertexTo);
}

void PushRelabel::Gap(int k) {
	for (int vertex = 0; vertex < NumberOfVertices; vertex++) {
		if (Distance[vertex] < k)
			continue;
		Count[Distance[vertex]]--;
		Distance[vertex] = std::max(Distance[vertex], NumberOfVertices + 1);
		Count[Distance[vertex]]++;
		Enqueue(vertex);
	}
}

void PushRelabel::Relabel(int vertex) {
	Count[Distance[vertex]]--;
	Distance[vertex] = 2 * NumberOfVertices;
	for (int i = 0; i < Graph[vertex].size(); i++)
		if (Graph[vertex][i].Capacity - Graph[vertex][i].FlowValue > 0)
			Distance[vertex] = std::min(Distance[vertex], Distance[Graph[vertex][i].VertexTo] + 1);
	Count[Distance[vertex]]++;
	Enqueue(vertex);
}

void PushRelabel::Discharge(int vertex) {
	for (int i = 0; Excess[vertex] > 0 && i < Graph[vertex].size(); i++)
		Push(Graph[vertex][i]);
	if (Excess[vertex] > 0) {
		if (Count[Distance[vertex]] == 1)
			Gap(Distance[vertex]);
		else
			Relabel(vertex);
	}
}

int PushRelabel::GetMaxFlow(int source, int sink) {
	Count[0] = NumberOfVertices - 1;
	Count[NumberOfVertices] = 1;
	Distance[source] = NumberOfVertices;
	Active[source] = Active[sink] = true;
	for (int i = 0; i < Graph[source].size(); i++) {
		Excess[source] += Graph[source][i].Capacity;
		Push(Graph[source][i]);
	}

	while (!Queue.empty()) {
		int vertex = Queue.front();
		Queue.pop();
		Active[vertex] = false;
		Discharge(vertex);
	}

	int result = 0;
	for (int i = 0; i < Graph[source].size(); i++)
		result += Graph[source][i].FlowValue;
	return this->MaxFlowResult = result;
}

int PushRelabel::FindStartFlight(int u, int N) {
	int flag;

	do {
		int v = u + N;
		flag = 0;
		for (auto& e : Graph[v])
			if (e.FlowValue == -1) {
				u = e.VertexTo;
				flag = 1;
				break;
			}
	} while (flag);

	return u;
}

int* PushRelabel::GetPlan() {
	std::vector< std::vector<int> > plans;
	std::vector<bool> visisted;
	int* planPtr = NULL;
	int N = NumberOfVertices / 2 - 1;

	if (MaxFlowResult == -1) { // chưa chạy max flow
		MaxFlowResult = GetMaxFlow(0, NumberOfVertices - 1);
	}

	//for (auto& v : Graph) {
	//	for (auto& e : v) {
	//		if (e.FlowValue != 0)
	//			std::cout << e.VertexFrom << " " << e.VertexTo << " " << e.FlowValue << "\n";
	//	}
	//}

	visisted.assign(N + 1, false);
	for (int i = 1; i <= N; i++)
		if (!visisted[i]) {
			int u = FindStartFlight(i, N);

			std::vector<int> tmp;
			int flag = 1;
			while (flag) {
				visisted[u] = true;
				tmp.push_back(u);
				flag = 0;
				for (auto& e : Graph[u])
					if (e.FlowValue == 1) {
						u = e.VertexTo - N;
						flag = 1;
						break;
					}
			}

			plans.push_back(tmp);
		}
	
	//for (auto& v : plans) {
	//	for (auto& e : v)
	//		std::cout << e << " ";
	//	std::cout << "\n";
	//}

	planPtr = new int[N + plans.size() + 1];
	planPtr[0] = plans.size();

	int id = 1;
	for (auto& v : plans) {
		planPtr[id++] = v.size();
		for (auto& e : v)
			planPtr[id++] = e;
	}

	return planPtr;
}