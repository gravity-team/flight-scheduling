#ifndef _C_PUSH_RELABEL_H_
#define _C_PUSH_RELABEL_H_

#include <vector>
#include <queue>
#include <algorithm>
#include <iostream>

struct Edge {
	int VertexFrom;
	int VertexTo;
	int Capacity;
	int FlowValue;
	int EdgeIndex;

	Edge(int = 0, int = 0, int = 0, int = 0, int = 0);
};

class PushRelabel {
private:
	int NumberOfVertices;
	int MaxFlowResult;
	std::vector< std::vector<Edge> > Graph;
	std::vector<int> Excess;
	std::vector<int> Distance;
	std::vector<bool> Active;
	std::vector<int> Count;
	std::queue<int> Queue;

	void Enqueue(int);
	void Push(Edge&);
	void Gap(int);
	void Relabel(int);
	void Discharge(int);

	int FindStartFlight(int, int);
public:
	PushRelabel(int = 0);
	void AddEdge(int, int, int);
	int GetMaxFlow(int, int);
	int* GetPlan();
};

#endif