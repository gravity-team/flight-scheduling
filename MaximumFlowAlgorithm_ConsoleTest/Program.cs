﻿//using MaximumFlowAlgorithm;

namespace MaximumFlowAlgorithm_ConsoleTest
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    /**************************************************************************************************/
        //    /* Tạo đồ thị 2 phía: mỗi bên N đỉnh, bên trái từ 1 đến N, bên phải từ N+1 đến 2N                 */
        //    /* Trong đồ thị ban đầu, nếu (u, v) thì thêm cạnh (u, v+N) vào đồ thị 2 phía                      */
        //    /* Sau đó tạo thêm 2 đỉnh 0 và 2N+1, nối 0 với N đỉnh bên trái, nối N đỉnh bên phải với 2N+1      */
        //    /* Chạy Maximum flow từ 0 đến 2N+1                                                                */
        //    /* Số chuyến bay cần tối thiểu là N-maxFlow.                                                      */
        //    /**************************************************************************************************/

        //    /*
        //     *Example 1:
        //     *4 chuyến bay (1, 2, 3, 4)
        //     *1 đi đến được 3 và 4
        //     *2 đi đến được 4
        //     * 
        //     * Link: http://www.cs.princeton.edu/~wayne/cs423/lectures/max-flow-applications
        //     * Trang: 68
        //    */
        //    //int N = 4;
        //    //PushRelabelAlgorithm graph = new PushRelabelAlgorithm(2 * N + 2);
        //    //graph.AddEdge(0, 1, 1);
        //    //graph.AddEdge(0, 2, 1);
        //    //graph.AddEdge(0, 3, 1);
        //    //graph.AddEdge(0, 4, 1);
        //    //graph.AddEdge(5, 9, 1);
        //    //graph.AddEdge(6, 9, 1);
        //    //graph.AddEdge(7, 9, 1);
        //    //graph.AddEdge(8, 9, 1);
        //    //graph.AddEdge(1, 7, 1);
        //    //graph.AddEdge(1, 8, 1);
        //    //graph.AddEdge(2, 8, 1);

        //    //int maxFlow = graph.GetMaxFlow(0, 2 * N - 1);
        //    //Console.WriteLine("Maximum flow: {0}\nMinimum Aircraft: {1}", maxFlow, N - maxFlow);

        //    /*
        //     * Example 2:
        //     * 6 chuyến bay
        //     * 1 đi đến được 2 5 6
        //     * 2 đi đến được 5 6
        //     * 3 đi đến được 4 5 6
        //     * 4 đi đến được 5
        //     * 
        //     * Link: http://courses.cs.vt.edu/cs5114/spring2013/lectures/lecture19-network-flow-applications.pdf
        //     * Trang 110
        //    */
        //    //int N = 6;
        //    //PushRelabelAlgorithm graph = new PushRelabelAlgorithm(2 * N + 2);
        //    //graph.AddEdge(0, 1, 1);
        //    //graph.AddEdge(0, 2, 1);
        //    //graph.AddEdge(0, 3, 1);
        //    //graph.AddEdge(0, 4, 1);
        //    //graph.AddEdge(0, 5, 1);
        //    //graph.AddEdge(0, 6, 1);
        //    //graph.AddEdge(7, 13, 1);
        //    //graph.AddEdge(8, 13, 1);
        //    //graph.AddEdge(9, 13, 1);
        //    //graph.AddEdge(10, 13, 1);
        //    //graph.AddEdge(11, 13, 1);
        //    //graph.AddEdge(12, 13, 1);
        //    //graph.AddEdge(1, 8, 1);
        //    //graph.AddEdge(1, 11, 1);
        //    //graph.AddEdge(1, 12, 1);
        //    //graph.AddEdge(2, 11, 1);
        //    //graph.AddEdge(2, 12, 1);
        //    //graph.AddEdge(3, 10, 1);
        //    //graph.AddEdge(3, 11, 1);
        //    //graph.AddEdge(3, 12, 1);
        //    //graph.AddEdge(4, 11, 1);

        //    //int maxFlow = graph.GetMaxFlow(0, 2 * N - 1);
        //    //Console.WriteLine("Maximum flow: {0}\nMinimum Aircraft: {1}", maxFlow, N - maxFlow);
        //}


        /******************************************************************************************************/
        /*                                                                                                    */
        /*                                                                                                    */
        /*                                    TEST WITH CPLUSPLUS CLASS                                       */
        /*                                                                                                    */
        /*                                                                                                    */
        /******************************************************************************************************/
        static void Main(string[] args)
        {
            /**************************************************************************************************/
            /* Tạo đồ thị 2 phía: mỗi bên N đỉnh, bên trái từ 1 đến N, bên phải từ N+1 đến 2N                 */
            /* Trong đồ thị ban đầu, nếu (u, v) thì thêm cạnh (u, v+N) vào đồ thị 2 phía                      */
            /* Sau đó tạo thêm 2 đỉnh 0 và 2N+1, nối 0 với N đỉnh bên trái, nối N đỉnh bên phải với 2N+1      */
            /* Chạy Maximum flow từ 0 đến 2N+1                                                                */
            /* Số chuyến bay cần tối thiểu là N-maxFlow.                                                      */
            /**************************************************************************************************/

            /*
             *Example 1:
             *4 chuyến bay (1, 2, 3, 4)
             *1 đi đến được 3 và 4
             *2 đi đến được 4
             * 
             * Link: http://www.cs.princeton.edu/~wayne/cs423/lectures/max-flow-applications
             * Trang: 68
            */
            //int N = 4;
            //PushRelabelWrapper.CPushRelabelWrapper graph = new PushRelabelWrapper.CPushRelabelWrapper(2 * N + 2);
            //graph.AddEdge(0, 1, 1);
            //graph.AddEdge(0, 2, 1);
            //graph.AddEdge(0, 3, 1);
            //graph.AddEdge(0, 4, 1);
            //graph.AddEdge(5, 9, 1);
            //graph.AddEdge(6, 9, 1);
            //graph.AddEdge(7, 9, 1);
            //graph.AddEdge(8, 9, 1);
            //graph.AddEdge(1, 7, 1);
            //graph.AddEdge(1, 8, 1);
            //graph.AddEdge(2, 8, 1);

            //int maxFlow = graph.GetMaxFlow(0, 2 * N + 1);
            //Console.WriteLine("Maximum flow: {0}\nMinimum Aircraft: {1}", maxFlow, N - maxFlow);

            /*
             * Example 2:
             * 6 chuyến bay
             * 1 đi đến được 2 5 6
             * 2 đi đến được 5 6
             * 3 đi đến được 4 5 6
             * 4 đi đến được 5
             * 
             * Link: http://courses.cs.vt.edu/cs5114/spring2013/lectures/lecture19-network-flow-applications.pdf
             * Trang 110
             * Có thay đổi cạnh
            */
            //List<Flight> flights;
            //List<FlightsProvincesModel> flightProvinceModels;
            //Dictionary<int, int> hashes = new Dictionary<int, int>();
            //using (var db = new Context())
            //{
            //    flights = db.Flights.ToList();
            //    flightProvinceModels = db.FlightsProvincesModels.ToList();
            //}
            //int N = flights.Count;
            //PushRelabelWrapper.CPushRelabelWrapper graph = new PushRelabelWrapper.CPushRelabelWrapper(2 * N + 2);

            //for (int i = 0; i < N; i++)
            //{
            //    hashes.Add(flights[i].Id, i + 1);
            //}

            //foreach (var f in flights)
            //{
            //    /* Đồ thị này dễ hình dung sẽ có 4 lớp
            //     *                 /- Đỉnh 1            Đỉnh 6 (ảo của 1)  -\
            //     *                /-- Đỉnh 2            Đỉnh 7 (ảo của 2)  --\
            //     *      Đỉnh 0 --<--- Đỉnh 3            Đỉnh 8 (ảo của 3)  --->----- Đỉnh 11 (đích ảo)
            //     *                \-- Đỉnh 4            Đỉnh 9 (ảo của 4)  --/
            //     *                 \- Đỉnh 5            Đỉnh 10 (ảo của 5) -/
            //     * 
            //     * Nếu flight i bay đến flight j được, thì thêm cạnh từ i đến (j+N) tức là đỉnh ảo của j ở lớp 3
            //    */
            //    graph.AddEdge(0, hashes[f.Id], 1);
            //    graph.AddEdge(hashes[f.Id] + N, 2 * N + 1, 1); // thiếu cạnh dẫn từ ảo đến đích)
            //    var currentDestination = flightProvinceModels.Where(fpm => fpm.FlightId == f.Id && !fpm.IsSource).FirstOrDefault();
            //    var listRelatedDestination = flightProvinceModels.Where(fpm => fpm.ProvinceId == currentDestination.ProvinceId && fpm.IsSource).ToList();
            //    foreach (var rd in listRelatedDestination)
            //    {
            //        if (f.EndTime <= rd.Flight.StartTime)
            //        {
            //            graph.AddEdge(hashes[f.Id], hashes[rd.Flight.Id] + N, 1); //nối với đỉnh ảo của rd chứ không phải là rd.
            //        }
            //    }
            //}
            //PushRelabelWrapper.CPushRelabelWrapper graph = new PushRelabelWrapper.CPushRelabelWrapper(14);
            //int N = 6;
            //graph.AddEdge(0, 1, 1);
            //graph.AddEdge(0, 2, 1);
            //graph.AddEdge(0, 3, 1);
            //graph.AddEdge(0, 4, 1);
            //graph.AddEdge(0, 5, 1);
            //graph.AddEdge(0, 6, 1);
            //graph.AddEdge(7, 13, 1);
            //graph.AddEdge(8, 13, 1);
            //graph.AddEdge(9, 13, 1);
            //graph.AddEdge(10, 13, 1);
            //graph.AddEdge(11, 13, 1);
            //graph.AddEdge(12, 13, 1);
            //graph.AddEdge(1, 8, 1);
            //graph.AddEdge(1, 11, 1);
            //graph.AddEdge(1, 12, 1);
            //graph.AddEdge(2, 11, 1);
            //graph.AddEdge(2, 12, 1);
            //graph.AddEdge(3, 10, 1);
            //graph.AddEdge(3, 11, 1);
            //graph.AddEdge(3, 12, 1);
            //graph.AddEdge(4, 11, 1);

            //int maxFlow = graph.GetMaxFlow(0, 2*N+1);
            //Console.WriteLine("Maximum flow: {0}\nMinimum Aircraft: {1}", maxFlow, N - maxFlow);

            //// cách lấy ra từng chuyến bay
            //List<List<int>> plan = new List<List<int>>();
            //unsafe
            //{
            //    int* planPtr = graph.GetPlan();
            //    int numOfPlans = planPtr[0];
            //    int id = 1;
            //    for (int i = 0; i < numOfPlans; i++)
            //    {
            //        int num = planPtr[id++];
            //        List<int> curPlan = new List<int>();
            //        for (int j = 0; j < num; j++)
            //            curPlan.Add(planPtr[id++]);
            //        plan.Add(curPlan);
            //    }

            //    graph.FreeMemory(planPtr);
            //}

            //foreach (List<int> v in plan)
            //{
            //    foreach (int e in v)
            //        Console.Write(e + " ");
            //    Console.WriteLine();
            //}
        }
    }
}