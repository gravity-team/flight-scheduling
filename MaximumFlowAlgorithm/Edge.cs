﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaximumFlowAlgorithm
{
    /// <summary>
    /// class Edge trong Push-Relabel
    /// biểu diễn cạnh có hướng
    /// </summary>
    class Edge
    {
        /// <summary>
        /// VertextFrom: đỉnh nguồn (đỉnh bắt đầu của cạnh)
        /// </summary>
        public int VertexFrom { get; set; }

        /// <summary>
        /// VertexTo: đỉnh đích (đỉnh kết thúc của cạnh)
        /// </summary>
        public int VertexTo { get; set; }

        /// <summary>
        /// Capacity: Sức chứa tối đa, khả năng thông luồng tối đa của cạnh
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// FlowValue: Giá trị luồng thực tế của cạnh
        /// </summary>
        public int FlowValue { get; set; }

        /// <summary>
        /// Index: chỉ số của cạnh trong thuật toán Push Relabel
        /// </summary>
        public int EdgeIndex { get; set; }
    }
}
