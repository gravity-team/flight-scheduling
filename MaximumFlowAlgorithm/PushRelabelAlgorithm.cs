﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaximumFlowAlgorithm
{
    /// <summary>
    /// Thuật toán Push-Relabel (hay Preflow-Push)
    /// Sử dụng Gap Heuristic để bỏ qua các bước relabel vô nghĩa.
    /// Độ phức tạp: O(|V|^3)
    /// </summary>
    public class PushRelabelAlgorithm
    {
        private int NumberOfVertices { get; set; }
        private List<List<Edge>> Graph { get; set; }
        private List<int> Excess { get; set; }
        private List<int> Distance { get; set; }
        private List<bool> Active { get; set; }
        private List<int> Count { get; set; }
        private Queue<int> MyQueue { get; set; }

        public PushRelabelAlgorithm(int numberOfVertices)
        {
            NumberOfVertices = numberOfVertices;
            this.Graph = new List<List<Edge>>();
            this.Excess = new List<int>();
            this.Distance = new List<int>();
            this.Active = new List<bool>();
            this.Count = new List<int>();
            this.MyQueue = new Queue<int>();
            for (int i = 0; i < NumberOfVertices; i++)
            {
                this.Graph.Add(new List<Edge>());
                this.Excess.Add(0);
                this.Distance.Add(0);
                this.Active.Add(false);
                this.Count.Add(0);
                this.Count.Add(0); // mảng Count cần 2*|V| phần tử
            }
        }

        //public PushRelabelAlgorithm()
        //{
        //    NumberOfVertices = 0;
        //    this.Graph = new List<List<Edge>>();
        //    this.Excess = new List<int>();
        //    this.Distance = new List<int>();
        //    this.Active = new List<bool>();
        //    this.Count = new List<int>();
        //    this.MyQueue = new Queue<int>();
        //}

        public void AddEdge(int vertextFrom, int vertexTo, int capacity)
        {
            Graph[vertextFrom].Add(new Edge
            {
                VertexFrom = vertextFrom,
                VertexTo = vertexTo,
                Capacity = capacity,
                FlowValue = 0,
                EdgeIndex = Graph[vertexTo].Count()
            });

            if (vertextFrom == vertexTo)
                Graph[vertextFrom].Last().EdgeIndex++;

            Graph[vertexTo].Add(new Edge
            {
                VertexFrom = vertexTo,
                VertexTo = vertextFrom,
                Capacity = capacity,
                FlowValue = 0,
                EdgeIndex = Graph[vertextFrom].Count() - 1
            });
        }

        private void Enqueue(int vertex)
        {
            if (!Active[vertex] && Excess[vertex] > 0)
            {
                Active[vertex] = true;
                MyQueue.Enqueue(vertex);
            }
        }

        private void Push(ref Edge e)
        {
            int flowValue = Math.Min(Excess[e.VertexFrom], e.Capacity - e.FlowValue);
            if (Distance[e.VertexFrom] <= Distance[e.VertexTo] || flowValue == 0)
                return;
            e.FlowValue += flowValue;
            Graph[e.VertexTo][e.EdgeIndex].FlowValue -= flowValue;
            Excess[e.VertexTo] += flowValue;
            Excess[e.VertexFrom] -= flowValue;
            Enqueue(e.VertexTo);
        }

        private void Push(int v, int i)
        {
            
            int flowValue = Math.Min(Excess[Graph[v][i].VertexFrom], Graph[v][i].Capacity - Graph[v][i].FlowValue);
            if (Distance[Graph[v][i].VertexFrom] <= Distance[Graph[v][i].VertexTo] || flowValue == 0)
                return;
            Graph[v][i].FlowValue += flowValue;
            Graph[Graph[v][i].VertexTo][Graph[v][i].EdgeIndex].FlowValue -= flowValue;
            Excess[Graph[v][i].VertexTo] += flowValue;
            Excess[Graph[v][i].VertexFrom] -= flowValue;
            Enqueue(Graph[v][i].VertexTo);
        }

        private void Gap(int k)
        {
            for (int vertex = 0; vertex < NumberOfVertices; vertex++)
            {
                if (Distance[vertex] < k) continue;
                Count[Distance[vertex]]--;
                Distance[vertex] = Math.Max(Distance[vertex], NumberOfVertices + 1);
                Count[Distance[vertex]]++;
                Enqueue(vertex);
            }
        }

        private void Relabel(int vertex)
        {
            Count[Distance[vertex]]--;
            Distance[vertex] = 2 * NumberOfVertices;
            for (int i = 0; i < Graph[vertex].Count(); i++)
                if (Graph[vertex][i].Capacity - Graph[vertex][i].FlowValue > 0)
                    Distance[vertex] = Math.Min(Distance[vertex], Distance[Graph[vertex][i].VertexTo] + 1);
            Count[Distance[vertex]]++;
            Enqueue(vertex);
        }

        private void Discharge(int vertex)
        {
            for (int i = 0; Excess[vertex] > 0 && i < Graph[vertex].Count(); i++)
                //Push(ref Graph.ElementAt(vertex).ElementAt(i));
                Push(vertex, i);
            if (Excess[vertex] > 0)
            {
                if (Count[Distance[vertex]] == 1)
                    Gap(Distance[vertex]);
                else
                    Relabel(vertex);
            }
        }

        /// <summary>
        /// Run Push-Relabel algorithm
        /// </summary>
        /// <param name="source">Source vertex</param>
        /// <param name="sink">Sink vertex</param>
        /// <returns></returns>
        public int GetMaxFlow(int source, int sink)
        {
            Count[0] = NumberOfVertices - 1;
            Count[NumberOfVertices] = 1;
            Distance[source] = NumberOfVertices;
            Active[source] = Active[sink] = true;
            for (int i = 0; i < Graph[source].Count(); i++)
            {
                Excess[source] += Graph[source][i].Capacity;
                Push(source, i);
            }

            while (MyQueue.Count() > 0)
            {
                int vertex = MyQueue.Dequeue();
                Active[vertex] = false;
                Discharge(vertex);
            }

            int result = 0;
            for (int i = 0; i < Graph[source].Count(); i++)
                result += Graph[source][i].FlowValue;
            return result;
        }

        /// <summary>
        /// Alias to GetMaxFlow function, use GetMaxFlow is better
        /// </summary>
        /// <param name="source">Source vertex</param>
        /// <param name="sink">Sink vertex</param>
        /// <returns></returns>
        public int RunAlgorithm(int source, int sink)
        {
            return this.GetMaxFlow(source, sink);
        }
    }
}